all : presentations

presentations: vhdl_timing.pdf state_machine.pdf

%.pdf : %.tex
	latexmk -pdf -cd -f -interaction=nonstopmode -shell-escape -synctex=1 $^

clean :
	rm -rf _minted* *.aux *.fdb_latexmk *.fls *.log *.nav *.out *.pdf *.snm *.toc *.vrb *.synctex.gz
	