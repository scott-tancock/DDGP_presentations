\documentclass{beamer}
\usetheme{Berlin}
\usecolortheme{beaver}
\usepackage{tikz}
\usepackage{minted}

\begin{document}
\AtBeginSection[]{
  \begin{frame}
    \frametitle{Table of Contents}
    \tableofcontents[currentsection]
  \end{frame}
}
\title{Digital Design Group Project}
\subtitle{Lecture 1: VHDL and its timing model}
\author{Scott~Tancock\inst{1}}
\institute{
  \inst{1}%
  School of Computer Science, Electronic Engineering and Engineering Mathematics\\
  University of Bristol, United Kingdom
}
\subject{Electronic Engineering}
\date{TB2, 2019-2020}

\frame{\titlepage}

\section[Overview]{VHDL - An Overview}

\begin{frame}
  \frametitle{What is VHDL?}
  \begin{itemize}
  \item VHDL is a Hardware Definition Language (HDL).  Its aim is to describe and model (electronic) hardware.
  \item VHDL is a simulated language - the language describes a construct, and then a simulator determines how it behaves over time.
  \item VHDL has a relaxed timing model - some things may execute in an unexpected order.
  \item VHDL is a mixture of imperative and declarative - you either describe what you want or how you want it done, albeit at a much lower level than other languages such as C and Java.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{VHDL Architecture}
  \begin{itemize}
  \item VHDL is made to model hardware, so its syntax must somehow relate to hardware.
  \item VHDL is made out of 5 main components:
    \begin{itemize}
    \item Modules
    \item Signals
    \item Processes
    \item Variables
    \item Statements
    \end{itemize}
  \item VHDL defines one of its modules as the ``top'' module.  The top module is where it starts the simulation, a bit like ``main(...)'' in C or Java.
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Modules}
  \begin{itemize}
  \item A module corresponds to a block of hardware.
    \begin{itemize}
    \item Such as a processor, a counter or a flip-flop.
    \end{itemize}
  \item Each module has an external appearance, called an entity.
  \item Each module has a number of signals (part of the entity).
    \begin{itemize}
    \item These signals may be inputs and outputs.
    \item Or they may be internal signals (can only be seen inside the module).
    \end{itemize}
  \item Each module has a number of architectures, which define individual implementations of the module.
  \item Each module has a number of processes (part of an architecture).
    \begin{itemize}
    \item These processes are able to see all of the signals in the module (including inputs and outputs).
    \item These processes cannot see each others' variables.
    \end{itemize}
  \item Each module has a number of sub-modules.
    \begin{itemize}
    \item This allows for a hierarchical design structure and the re-use of components in multiple places.
    \item The use of one module from within another is analogous to function calls or object instantiation in procedural or object-oriented languages.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Signals}
  \begin{itemize}
  \item A signal something that holds a value.
  \item They can be considered similar to global variables in other programming languages.
  \item They are contained within a module.
  \item All processes within the module can see them.
  \item All inputs and outputs are signals, but some signals are instead internal to the module.
  \item Some people equate signals to wires.  This is a dangerous link to make, we'll see why later on.
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Processes}
  \begin{itemize}
  \item Processes are equivalent to individual pieces of logic.
    \begin{itemize}
    \item They read from some input signals and write to some output signals.
    \end{itemize}
  \item Processes are described by some variables and a sequence of statements.
    \begin{itemize}
    \item The sequence of statements are executed sequentially, but their effects may not happen immediately.
    \item The process may use variables to allow you to describe a complex piece of logic in stages.
    \end{itemize}
  \item Processes have triggers to tell them when to re-evaluate their outputs.
    \begin{itemize}
    \item If two processes trigger at the same time, the process to be evaluated first is not defined (i.e. random).
    \item \textit{Do not expect a process earlier on in the code (further up the file) to execute before a later one.}
    \item The process has a sensitivity list, which lists all of the process's triggers.
    \item If there is no sensitivity list, then all inputs (to the process) are triggers.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Variables}
  \begin{itemize}
  \item Variables also hold values.
  \item They can also be considered similar to local variables in other programming languages.
  \item Variables are contained within a process.
  \item Only the process that contains them can see them.
  \item Variables often have no hardware equivalent, but are useful in describing complex hardware.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Statements}
  \begin{itemize}
  \item Statements are inidivdual operations on signals and/or variables.
  \item Statements may update a target signal through a blocking (\mintinline{vhdl}{:=}) or non-blocking (\mintinline{vhdl}{<=}) assignment
    \begin{itemize}
    \item A blocking assignment will update the target immediately.  The target's new value will be seen by the next statement.
    \item Blocking assignments should only be used on variable targets.
    \item A non-blocking assignment will update the target once all waiting processes finish executing, either due to reaching the end or due to a \mintinline{vhdl}{WAIT} statement, or after a specified time delay (\mintinline{vhdl}{AFTER} statement).
    \item Non-blocking assignments are best used on signal targets.
    \end{itemize}
  \item Statements are evaluated sequentially within a process.
  \item A statement may cause the execution of a process to stop, for example a \mintinline{vhdl}{WAIT} statement.
  \end{itemize}
\end{frame}

\section[Timing]{The VHDL Timing Model}

\begin{frame}
  \frametitle{VHDL, Simulation Time and Real Time}
  \begin{itemize}
  \item VHDL is a hardware definition language, so the program that runs VHDL code is necessarily a simulator.
  \item A simulator models how the input object changes over time.
  \item Hence, the simulator must have a concept of time.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Real Time vs Simulation Time}
  \begin{itemize}
  \item Real time is time as we experience it.
  \item It is also known as wall time as we can measure it by looking at a clock on the wall.
  \item Time within the simulator (time as the VHDL model experiences it) is called simulation time, and is very different from the real time we experience.
  \item Simulation time may run faster or slower than real time.
  \item The speed at which the simulation time progresses is dependent on the amount of activity in the simulation.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Example: Simulation Time faster than Real Time}
  \begin{itemize}
  \item Example: Our top module (where the simulator starts) only has a single process, which inverts a signal, ``clk'' once every second of simulation time.
  \item This process takes 100ns of real time to run each time it does some work (i.e: once every 1s of simulation time).
  \item If we were to run our simulator for 1s of real time, it could run the process 10,000,000 times.
  \item As each run of the process is 1s of simulation time, this means the simulation time could reach 10,000,000 seconds!
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Example: Simulation Time slower than Real Time}
  \begin{itemize}
  \item Example: Our top module has one process, which inverts ``clk'' once every nanosecond of simulation time.
  \item This process also takes 100ns of real time to run.
  \item If we were to run our simulator for 1s of real time, the process would execute 10,000,000 times.
  \item As each run of the process is 1ns of simulation time, the simulator would reach 10 milliseconds.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\mintinline{vhdl}{AFTER} statements}
  \begin{itemize}
  \item An \mintinline{vhdl}{AFTER} statement tells the simulator to update the target after a defined span of simulation time.
  \item \mintinline{vhdl}{z <= x+y AFTER 10ns}
  \item The above statement will update Z 10ns after the process finishes executing.
  \item Processes execute \textit{instantaneously} (in simulation time), so the assignment will happen 10ns from the current time.
  \item The value of x+y is calculated immediately, so we won't see future updates to x and y UNLESS they are in the sensitivity list.
  \item \mintinline{vhdl}{AFTER} statements do have a hardware equivalent, but it's not easy to create, so these should only be used for the simulation of logic delays.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\mintinline{vhdl}{WAIT for} statements}
  \begin{itemize}
  \item A \mintinline{vhdl}{WAIT for} statement tells the simulator to pause the execution of the process for a defined amount of time.
  \item \mintinline{vhdl}{WAIT for 30ns}
  \item When a \mintinline{vhdl}{WAIT} statement is encountered, the process is suspended, meaning that once all other waiting processes have executed, non-blocking assignments can be performed.
  \item \mintinline{vhdl}{WAIT for} statements do have a hardware equivalent, but it's not easy to create, so \textbf{avoid them at all costs}.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\mintinline{vhdl}{WAIT on} statements}
  \begin{itemize}
  \item Wait statements can also be used to pause until something happens (a signal changes).
  \item \mintinline{vhdl}{WAIT on x,y}
  \item This is a bit like inserting a sensitivity list in the middle of your process.
  \item The process will continue executing when any of the signals changes.
  \item Hardware equivalents are difficult to determine, so \textbf{avoid them except for in testbenches}.
  \end{itemize}
\end{frame}

\section[Syntax]{Using VHDL}

\begin{frame}
  \frametitle{Libraries}
  \begin{itemize}
  \item An important part of VHDL is the standard libraries.
  \item These implement common functionality within the language so that you can avoid re-inventing the wheel.
  \item Libraries define data types and operations on those data types.
    \begin{itemize}
    \item \mintinline{vhdl}{ieee.std_logic_1164.all} defines the standard data types \mintinline{vhdl}{std_logic} and \mintinline{vhdl}{std_logic_vector}
    \item \mintinline{vhdl}{ieee.numeric_std.all} define the arithmetic operators (\mintinline{vhdl}{+},\mintinline{vhdl}{-},\mintinline{vhdl}{*},\mintinline{vhdl}{/}) on \mintinline{vhdl}{std_logic_vector}.
    \end{itemize}
  \item First, you need to declare a library (\mintinline{vhdl}{library ieee;}) then you include parts of that library (\mintinline{vhdl}{use ieee.std_logic_1164.all;}).
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
  \frametitle{Entity Declaration}
  \begin{itemize}
  \item Once we've included some libraries, we then declare the entity (how the module looks to the outside world) in this file.
  \item \mintinline{vhdl}{entity my_adder is}
  \item This is optionally followed by a set of generics (a bit like Java's generic data types or C++'s templates)
  \item \mintinline{vhdl}{generic (BITS : integer; SLEW_RATE: string)}
  \item This is optionally (almost always) followed by a set of ports (inputs and outputs, a bit like Java's and C's function parameters and return types)
  \item
    \begin{minted}{vhdl}
      port (
        X, Y : in std_logic_vector(31 downto 0);
        Z, C : out std_logic
      );
    \end{minted}
  \item Finally, we end the entity declaration.
  \item \mintinline{vhdl}{end entity my_adder;}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
  \frametitle{Architecture Declaration}
  \begin{itemize}
  \item Now we've declared the entity, we can declare a number of architectures.
  \item An architecture is an implementation of the entity, following a particular style and design specification.
  \item First we name the architecture.
  \item \mintinline{vhdl}{architecture rtl of my_adder is}
  \item Then we declare any signals and custom types we need.
  \item \begin{minted}{vhdl}
      signal w : std_logic_vector(3 downto 0);
      type state_type is (IDLE, PROCESSING, DONE);
    \end{minted}
  \item Then we begin the architecture.
  \item \mintinline{vhdl}{begin}
  \item The processes go after the \mintinline{vhdl}{begin} but before the end of the architecture.
  \item \mintinline{vhdl}{end architecture rtl;}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Process declarations}
  \begin{itemize}
  \item Processes are declared similarly to signals and variables, but with an optional sensitivity list in parentheses ``()''
  \item \mintinline{vhdl}{add_numbers : process(X)} or \mintinline{vhdl}{xor_numbers : process}
  \item The process declaration is optionally followed by the declaration of any local variables.
  \item
    \begin{minted}{vhdl}
      variable i : integer;
      variable bitmask : std_logic_vector;
    \end{minted}  
  \item The variables are then followed by a \mintinline{vhdl}{begin}.
  \item At this point the statements that describe your process can be written, followed by an \mintinline{vhdl}{end process;}.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Module instantiations}
  \begin{itemize}
  \item Modules are declared similarly to signals, except that they have a \mintinline{vhdl}{port map} and optional \mintinline{vhdl}{generic map}.
  \item \mintinline{vhdl}{adder1 : my_adder}
  \item The generic map gives values to the generics defined in the entity and is optional.
  \item \mintinline{vhdl}{generic map( BITS => 32, SLEW_RATE => 'FAST')}
  \item The port map is allows you to connect the submodule's inputs and outputs, and is required if the entity has ports.
  \item \textit{You should always give values to all inputs, while the outputs can be left unconnected.}
  \item \mintinline{vhdl}{port map( X => a, Y => b, Z => c);}
  \end{itemize}
\end{frame}

\section{Design Patterns}

\begin{frame}[allowframebreaks]
  \frametitle{Combinational Logic, Latches and Flip-Flops}
  \begin{itemize}
  \item All logic (that we care about) can be split into one of three categories: combinational, latches and flip-flops.
  \item Combinational logic has no conecpt of state.  The output is wholly dependant on the input, and giving the same input will always give the same output.
  \item Latches, on the other hand, do have a concept of state.  They have an enable, and an input, and when the enable is high, they will take the value of the input.  However, when the enable is low, they will maintain the value they had when the input was last high.
  \item Flip-flops also have state.  However, flip-flops have a clock and take the value of the input when the clock transitions from low to high,  when the clock is not transitioning from low to high, they maintain their state.
  \item In VHDL, \textbf{all three of these} are modelled by a process assigning to a signal.
    \begin{itemize}
    \item This is why assuming signal = wire is dangerous.
    \item Some would argue that the signal is the wire at the output of the logic, and that the process itself is the flip-flop/latch/combinational logic.
    \item This may be true when confortable with VHDL, but for beginners this is an unhelpful paradigm
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
  \frametitle{Combinational Logic}
  \begin{itemize}
  \item Combinational logic is modelled by a process assigning to a signal.
  \item The key differentiator is that \textbf{all the inputs} must be in the sensitivity list (if there is one).
  \item
    \begin{minted}{vhdl}
      add_proc : process(x, y)
      begin
          z <= x+y;
      end process;
    \end{minted}
  \item If you fail to put all variables in the sensitivity list, then it will (most likely) become a latch.  It is often easier to not include a sensitivity list at all (and let the simulator work it out for you).
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
  \frametitle{Flip-Flops}
  \begin{itemize}
  \item Flip-flops are also modelled by a process and signal.
  \item However, with a flip-flop, \textbf{you only update the output on the clock edge}.
  \item 
    \begin{minted}{vhdl}
      Z_ff : process(clk) begin
          if clk'event and clk='1' then
              Z_out <= Z;
          end if;
      end process;
  \end{minted}
  \item Here we see that only the clock (\mintinline{vhdl}{clk}) needs to appear in the sensitivity list as this is the only time the output is updated. An \mintinline{vhdl}{if} statement is used to determine if the clock is on a rising edge.
    \begin{itemize}
    \item \mintinline{vhdl}{clk'event} is only true if the signal \mintinline{vhdl}{clk} is changing at this point in time (rising or falling edge)
    \item \mintinline{vhdl}{clk='1'} is true when a signal is high or on the rising edge of the signal.
    \item These two conditions could have been replaced with \mintinline{vhdl}{rising_edge(clk)} with no difference.
    \end{itemize}
  \item This flip-flop takes the (combinational) signal \mintinline{vhdl}{Z} at the clock edge and outputs it to the signal \mintinline{vhdl}{Z_out} until the next clock edge.
  \item It doesn't matter if we put \mintinline{vhdl}{Z} in the sensitivity list, as the if condition stops the flip-flop from updating.  However, it will slow down the simulator!
    \begin{itemize}
    \item The simulator evaluates the process every time the sensitivity list changes, regardless of whether anything is going to change (as it doesn't know whether it will change or not yet)
    \item By introducing Z to the sensitivity list, the process must be evaluated when Z changes, even though the clk signal is not on a rising or falling edge (so it skips the body of the if statement).
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
  \frametitle{Latches}
  \begin{itemize}
  \item Latches are similar to flip-flops in that they hold state.
  \item However, where flip-flops remember the state at the last rising edge of the clock, latches remember state the last time the enable signal was high.
  \item This means that if your logic changes its output while the enable is still high, the latch will transmit this rather than the previous data.
  \item While latches are useful in some scenarios, you will most likely never want or need them in your designs.  If the tools tell you that you have a latch, you should treat this as an error condition.
  \item Some examples of latches in code are included for completeness.
  \item `explicit latch': Z must be in the sensitivity list for correct latch simulation.
    \begin{minted}{vhdl}
      Z_latch : process(enable, Z)
      begin
        if enable = '1' then
          Z_out <= Z;
        end if;
      end process;
    \end{minted}
  \item `not really a latch': latch in simulation but combinational in hardware implementation.
    \begin{minted}{vhdl}
      X_comb : process(Y)
      begin
        X <= Y or Z;
      end process;
    \end{minted}
  \item `implicit latch': an attempt to hold state in combinational logic.
    \begin{minted}{vhdl}
      output_comb : process(Y, Z)
      begin
        --Update Z output if Y is 0XCAFEBABE
        if Y = X"CAFEBABE" then
          Z_out <= Z;
        end if;
      end process;
    \end{minted}
  \item The `implicit latch' and `not really a latch' of the above examples were common errors when writing VHDL.
  \item Both will have your simulation and hardware implementation do different things.  Don't fall into this trap!
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
  \frametitle{Comments and good coding style}
  \begin{itemize}
  \item When coding, always make sure to keep your code tidy and readable.
  \item One day, you'll have to go back to it and lament not keeping it in a nice state.
  \end{itemize}
  \begin{enumerate}
  \item The most important thing is comments.
    \begin{itemize}
    \item Comments are lines of text that have no impact on the functionality of the code.
    \item They start with two dashes (minus signs)
    \item \mintinline{vhdl}{--This is a comment}
    \item They allow you to explain the how and why of your code - the high-level overview and the reasons for making the choices you made.
    \item Comments turn confusing code into intelligent code.  \textit{They also get you extra marks}.
    \end{itemize}
  \item After that is variable and state naming.
    \begin{itemize}
    \item Naming your variables and states properly makes it easier for the reader to understand what's going on.
    \item Naming a variable \mintinline{vhdl}{energy_accumulator} rather than \mintinline{vhdl}{a115}.
    \item Naming your states \mintinline{vhdl}{(IDLE,CAPTURING,PROCESSING,OUTPUT_ACC)} rather than \mintinline{vhdl}{(S0,S1,S2,S3)}
    \end{itemize}
  \item Finally, indent properly.
    \begin{itemize}
    \item When you go a level deeper into your code, indent by a single \textit{unit}, when continuing your code on a new line, align with syntax.
    \item A \textit{unit} may be a tab or a number of spaces, but keep it consistent.  If you mix spaces and tabs, it will turn into a mess on another person's computer.
    \item Indentation by level:
      \begin{minted}{vhdl}
        proc : process(a,b,c)
        begin
          a_out <= a or b;
          b_out <= '0';
          c_out <= '1';
          if c = '1' then
            b_out <= b and a;
          else
            c_out <= b and a;
          end if;
        end process;
      \end{minted}
    \item Indentation by syntax:
      \begin{minted}{vhdl}
        type my_state is (IDLE, DET_A, DET_N1, DET_N2,
                          DET_N3, START, WAIT_DRDY,
                          PRINT_B1, PRINT_B2,
                          PRINT_SPACE);
      \end{minted}
    \end{itemize}
  \item There are tools which will automatically indent your code for you, here are just a few:
    \begin{itemize}
    \item Sublime Text 3 and its Smart VHDL package.
    \item Emacs and vhdl-mode.
    \item Vim and vim-hdl.
    \item Many others such as Gedit, Kate, Atom and Notepad++ (these may need plugins).
    \end{itemize}
  \end{enumerate}
\end{frame}

\begin{frame}[plain]
  \frametitle{That's All Folks!}
  \framesubtitle{Any questions?}
\end{frame}
  
\end{document}