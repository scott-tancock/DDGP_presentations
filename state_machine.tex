\documentclass{beamer}
\usetheme{Berlin}
\usecolortheme{beaver}
\usepackage{tikz}
\usepackage{minted}
\usetikzlibrary{math,calc,arrows.meta}

\begin{document}
\AtBeginSection[]{
  \begin{frame}
    \frametitle{Table of Contents}
    \tableofcontents[currentsection]
  \end{frame}
}
\title{Digital Design Group Project}
\subtitle{Lecture 2: State machines in VHDL}
\author{Scott~Tancock\inst{1}}
\institute{
  \inst{1}%
  School of Computer Science, Electronic Engineering and Engineering Mathematics\\
  University of Bristol, United Kingdom
}
\subject{Electronic Engineering}
\date{TB2, 2019-2020}

\frame{\titlepage}

\section[Recap]{Recap: What are state machines?}

\begin{frame}
  \frametitle{What are state machines?}
  \begin{itemize}
  \item State machines are devices which take an input and a state, and then transition to a new state and produce an output.  The new state becomes the input state and the cycle repeats.
  \item State machines allow us to perform sequential operations.  i.e. \textit{this} then \textit{that}.
  \item State machines form the core of any digital control or user interface.
  \item The most basic form of a state machine is a Finite State Machine (FSM).
  \item There are two models of an FSM: Mealy and Moore.
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Mealy and Moore machines (FSMs)}
  \begin{itemize}
  \item Named after George H. Mealy and Edward F. Moore, Mealy and Moore machines are both models of FSMs.
  \item Both are equivalent in terms of power (what they can do) but vary in terms of efficiency (resources required to do it).
  \item Both take an input and state, and produce a new state which they transition to.
  \item However, the output of a Moore machine is \textit{only dependent on the current state}, whereas the output of a Mealy machine is dependent on the \textit{current state and input}.
  \item For this reason, mealy machines have higher performance (quicker to convert a recognised sequence of inputs to an output) but have a higher complexity (the output must take into account the state and input rather than just the state.
  \item For a pattern recogniser, you will find that the Moore machine takes one more cycle and state than the mealy machine to recognise an input.  This is because there must be a dedicated state to signal that the pattern has been recognised (which doesn't exist in the Mealy machine).
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Looking beyond FSMs}
  \begin{itemize}
  \item FSMs are not all-powerful.  They only have certain patterns they can recognise.  These patterns can be denoted by regular expressions.
  \item In addition, their ability to store data is extremely limited - the data storage must be performed inside the state itself, so for every bit of stored data the number of states doubles.
  \item Therefore, we have more advanced machines that can accomplish more difficult tasks such as stack machines, linear-bounded automata and Turing machines.  We won't cover them here, but some of the extensions you see later on convert the FSMs into these more advanced machines.
  \item FSMs allow us to interact with a user, perform a sequence of actions, and many other things that would be difficult with pure logic.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Logic equivalent}
  \begin{figure}
    \resizebox{0.75\textwidth}{!}{
      \begin{tikzpicture}
        \draw[fill=cyan!20] (0,0) rectangle node[align=center]{Next\\State\\Logic} (2,3);
        \draw[fill=green!20] (4,0) rectangle node[align=center]{State\\Register} (6,3);
        \draw[fill=cyan!20] (8,0) rectangle node[align=center]{Output\\Logic} (10,3);
        \draw[-Latex] (2,1.5) -- coordinate[midway](next_mid) node[midway,anchor=north]{\footnotesize Next State} (4,1.5);
        \draw[-Latex] (6,1.5) -- coordinate[midway](state_mid) node[midway,anchor=south]{\footnotesize Current State} (8,1.5);
        \draw[-Latex] (10,1.5) -- (11,1.5) node[align=center, anchor=west]{Output\\Signals};
        \draw[Latex-] (0,1.5) -- (-1,1.5) node[align=center, anchor=east]{Input\\Signals};
        \draw[-Latex] (state_mid) -- +(0,-2) -| (1,0);
        \draw[red,-Latex] (next_mid) -- +(0,2) -| node[midway,anchor=south]{\footnotesize Mealy Only}(9,3);
        \draw[Latex-] (4,0.5) -- (3.5,0.5) node[align=center, anchor=east]{\footnotesize Clock\\\footnotesize Reset};
      \end{tikzpicture}
    }
    \caption{Block diagram of a state machine implemented in logic.  State Register is sequential; Next State Logic and Output Logic are combinational.}
    \label{fig:sm_block}
  \end{figure}
\end{frame}

\section[FSMs in VHDL]{Implementing FSMs in VHDL}

\begin{frame}
  \frametitle{Architecture of a state machine}
  \begin{itemize}
  \item When we create a state machine in vhdl, we build it from three components: the next state logic, the state transition logic and the output logic.
  \item The state transition logic is relatively simple - you assign the output of the next state logic to the current state on the clock edge (flip-flop style).
  \item The next state logic is a combinational process that contains the state and the inputs in the sensitivity list, and determines the next state from the current state, often using a combination of \mintinline{vhdl}{if} and \mintinline{vhdl}{case} statements.
  \item The output logic is split into multiple combinational or flip-flop processes, one per output, with the state (Moore) or state and inputs (Mealy) in the sensitivity list.
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
  \frametitle{Encoding states in binary}
  \begin{itemize}
  \item All the components of the state machine need to be mapped to logic and binary values, including the state.
  \item There are many different encoding schemes possible for mapping states to binary values, such as one-hot codes, sequential binary codes and grey codes.
  \item Each method has its advantages, so choosing one yourself is difficult.
  \item Therefore, VHDL has a method by which it can choose for you, known as an enumeration.
  \item
    \begin{minted}{vhdl}
      type state_type is (IDLE, PROCESSING, DONE);
      signal curr_state, next_state : state_type;
    \end{minted}
  \item This defines three states, IDLE, PROCESSING and DONE, which the compiler will encode in the most efficient manner.
  \item You can use the states you defined as though they are normal values.
  \item \mintinline{VHDL}{next_state <= PROCESSING;}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
  \frametitle{Implementing the state transition logic}
  \begin{itemize}
  \item The state transition logic is a simple flip-flop process.
  \item
    \begin{minted}{vhdl}
      state_ff : process(reset, clk)
        if reset = '1' then
          curr_state <= IDLE;
        else
          if clk'event and clk = '1' then
            curr_state <= next_state;
          end if;
        end if;
      end process;
    \end{minted}
  \item This version has an asynchronous reset that allows the logic to be returned to a known state in case of a fault.  This is important as the logic may be in an unknown state when it powers up.
  \item Alternatively, you could use a synchronous reset.
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
  \frametitle{Implementing the next state logic}
  \begin{itemize}
  \item The next state logic is a combinational process.  It usually starts with a set of default signal assignments to ensure the process remains combinational (no latches) which is followed by a \mintinline{vhdl}{case} statement with the current state as the selector.
  \item
    \begin{minted}{vhdl}
      next_state_comb : process(curr_state, inX, inY,
                                inZ)
      begin
        next_state <= curr_state;
        case curr_state is
          when IDLE =>
            if inZ = '1' then
              next_state <= PROCESSING;
            end if;
          when PROCESSING =>
            if inY >= X"100" then
              next_state <= DONE;
            else if inX = "0" then
              next_state <= IDLE;
            end if;
          when DONE =>
            next_state <= IDLE;
        end case;
      end process;
    \end{minted}
  \item Within each \mintinline{vhdl}{when} clause there is generally an if statement to determine whether to progress to the next state or another state.
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]
  \frametitle{Implementing the output logic}
  \begin{itemize}
  \item When implementing output logic, you should create a separate process for each output signal.
  \item
    \begin{minted}{vhdl}
      out_ack : process(curr_state)
      begin
        ack <= '0';
        if curr_state = DONE then
          ack <= '1';
        end if;
      end process;
    \end{minted}
    \item
      \begin{minted}{vhdl}
        out_val : process(curr_state, inX)
        begin
          val <= "0";
          if curr_state = DONE then
            val <= X"3" & inX;
          end if;
        end process;
      \end{minted}
      \item
      \begin{minted}{vhdl}
        out_ptr : process(reset, clk)
        if reset = '1' then
          ptr <= X"8000";
        elsif clk'event and clk = '1' then
          if curr_state = PROCESSING then
            ptr <= X"8000" + inY;
          end if;
        end if;
      end process;
    \end{minted}
  \end{itemize}
\end{frame}

\section[Extension]{Extending the state machines}

\begin{frame}[allowframebreaks]
  \frametitle{Counters}
  \begin{itemize}
    \item The most simple addition we can make to a state machine is to add a counter.
    \item This allows us to the count the number of occurences of a condition without an excessively large state machine.
    \item A counter has at least two inputs: a reset signal and an enable signal.
    \item In addition, it may have other resets, a set, or a count direction (count up or count down).
    \item The state machine of a simple counter is shown below.  It counts up while \textit{enable} is high and resets when \textit{reset} is high.
    \begin{figure}
      \caption{A counter represented as a state machine.}
      \label{fig:counter}
    \end{figure}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Interfacing with counters - Method 1}
  \begin{itemize}
    \item Method 1 is when the counter is autonomous from the main state machine.
    \item The counter operates based on the inputs and (possibly) the machine's state.
    \item The machine reads from the counter to perform state transitions.
    \item This method is conceptually easier but doesn't allow for the reuse of the counter easily.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Interfacing with counters - Method 2}
  \begin{itemize}
    \item Method 2 is when the counter is controlled by the main state machine.
    \item The main state machine controls the counter's enable and reset signals itself, then reads from the counter to perform state transitions.
    \item This method allows a single counter to be reused, but introduces latency (when using a Moore state machine) and complicates the main state machine.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Memory}
  \begin{itemize}
    \item Memory is more complicated to add to a a state machine.
    \item There are two types of memory - a register file, where you can read as many values as you need at any one time, and a RAM, where there is a limited number of accesses per clock cycle (equal to the number of \textit{ports}).
    \item Memory should always be implemented as a separate process.
    \item Preferrably, memory should be written with one access per cycle per process, with no more processes than there are ports on the RAM.  This will allow the memory to be converted efficiently to on-chip RAM blocks on an FPGA.
    \item If the synthesiser cannot recognise your code as a RAM, it will create an array of registers (register file) instead.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Interfacing with memory - Method 1 (Register File)}
  \begin{itemize}
    \item Method 1 is as a register file.
    \item This involves a single clocked process which updates some elements of an array each cycle.
    \item The choice of which registers to update may be up to the clocked process, or some combinational logic.
    \item The updating of multiple registers in a single clock cycle is what forces the synthesiser to generate a register file.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Interfacing with memory - Method 2}
  \begin{itemize}
    \item Method 2 is as a RAM unit.
    \item There will be a single clocked process that either reads or writes to/from \textbf{one} location in the memory on each clock cycle.
    \begin{itemize}
      \item In some cases, dual-port memory may be available on the FPGA, in which case you can have any of the following:
      \item A single process either reading or writing to two locations (true dual-port).
      \item A single process that reads from one location and writes to another (simple dual-port).
      \item Two processes, each either reading or writing to a location (true dual-port).
      \item Two processes, one reading and the other writing (simple dual-port).
      \item A single process, either reading or writing (single-port, two instances may fit inside a single piece of RAM).
    \item The read address, write address and next value will normally be determined by other processes.
    \item In order to meet timing and create fast RAM, the input address, enable signal and data input may need to be clocked (registered).
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Interfacing with memory - Method 3}
  \begin{itemize}
    \item Method 3 involves using the built-in memory (Block RAMs for Xilinx, Ultra RAMs for Intel) directly.
    \item This can be done either using a primitive macro (directly calling a physical block on the FPGA) or using an IP integrator.
    \item Your synthesis tools have language templates that show how to use a primitive macro.
    \item Your synthesis tools also have an IP wizard to configure built in memory, as well as to build some peripheral logic to make it easier to use.  When the IP has been generated, the tool will give you a template to show how to use the IP.
    \item Look in the relevant docs for the synthesis tool and IP, and attempt it yourself!
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{...And more}
  \begin{itemize}
    \item You can attach pretty much \textit{anything} to a state machine.
    \item Wired communications, wireless communications, arithmetic logic units etc.
    \item Depending on what you attach, the state machine can perform complicated tasks, but the state machine is always there for the same purpose: to control things.
    \item Be creative.
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \frametitle{That's All Folks!}
  \framesubtitle{Any questions?}
\end{frame}

\end{document}